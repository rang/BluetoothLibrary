//
//  BleKiApi.m
//  BluetoothCore
//
//  Created by wulanzhou on 16/6/15.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import "BLEKiApiObject.h"
#import "BLEKiUtils.h"

@implementation BLEKiApi

/** 设置app渠道。
 @param channel 渠道名称
 @return void.
 */
+ (void)setAppChannel:(NSString *)channel{
    [BLEKiUtils setValue:channel forKey:kBLELocalChannelKey];
}


/*! @brief 获取当前SDK的版本号
 *
 * @return 返回当前SDK的版本号
 */
+(NSString *) getApiVersion{

    return @"1.0";
}


/** 设置是否打印sdk的log信息, 默认NO(不打印log).
 @param value 设置为YES, SDK 会输出log信息可供调试参考. 除非特殊需要，否则发布产品时需改回NO.
 @return void.
 */
+ (void)setLogEnabled:(BOOL)value{

    [BLEKiUtils setBool:value forKey:kBLELocalShowLogKey];
}

@end
