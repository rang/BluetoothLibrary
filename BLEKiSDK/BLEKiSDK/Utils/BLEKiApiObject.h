//
//  BLEKiApiObject.h
//  BLEKiSDK
//
//  Created by wulanzhou on 16/6/16.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

@class CBPeripheral;
@class CBCentralManager;
@class BLEDeviceInfo;
@class BLEConnector;

extern NSString *const  kBLEStateChangedNotification;  //蓝牙状态变更
extern NSString *const  kBLEConnectSetupNotification;  //蓝牙建立连接
extern NSString *const  kBLEConnectFailedNotification; //蓝牙连接失败
extern NSString *const  kBLEConnectCloseNotification;  //蓝牙断开连接
extern NSString *const  kBLEScanCompletedNotification; //蓝牙扫描完成

typedef enum {
    BLEStateUnknown=0,   //蓝牙未知状态
    BLEStateOpen,        //蓝牙打开状态
    BLEStateClose        //蓝牙关闭状态
}BLEState;


typedef enum {
    BLESkySimStateUnKnown=0,     //sim卡为未知状态
    BLESkySimStateMainCard,      //sim卡为主卡状态（170卡状态）
    BLESkySimStateVirtualCard,   //sim卡为虚拟卡状态
    BLESkySimStateNetworkCard    //sim卡为上网状态
    
}BLESkySimState;


//蓝牙状态变更
typedef void (^BLECentralStateChangedBlock) (BLEState bleState);
//扫描到单个设备
typedef void (^BLECentralDeviceScanBlock) (BLEDeviceInfo *device);
//扫描到多个设备
typedef void (^BLECentralDeviceScanCompletedBlock) (NSArray *devices);
//蓝牙连接成功
typedef void (^BLECentralConnectSuccessBlock) (CBPeripheral *peripheral);
//蓝牙连接失败
typedef void (^BLECentralConnectFailedBlock) (CBPeripheral *peripheral,NSError *error);
//蓝牙断开连接
typedef void (^BLECentralDisconnectBlock) (CBPeripheral *peripheral,NSError *error);


#pragma mark -基本配置

/**
 *  api配置处理
 */
@interface BLEKiApi : NSObject

/** 设置app渠道。
 @param channel 渠道名称
 @return void.
 */
+ (void)setAppChannel:(NSString *)channel;

/*! @brief 获取当前SDK的版本号
 *
 * @return 返回当前SDK的版本号
 */
+ (NSString *) getApiVersion;

/** 设置是否打印sdk的log信息, 默认NO(不打印log).
 @param value 设置为YES, SDK 会输出log信息可供调试参考. 除非特殊需要，否则发布产品时需改回NO.
 @return void.
 */
+ (void)setLogEnabled:(BOOL)value;

@end


#pragma mark -蓝牙设备信息

@interface BLEDeviceInfo : NSObject

/**
 *  外围设备
 */
@property (nonatomic,strong) CBPeripheral *peripheral;

/**
 *  广播信息
 */
@property (nonatomic,strong) NSDictionary *advertisementData;

/**
 *  蓝牙信号
 */
@property (nonatomic,strong) NSNumber *rssi;

/**
 * 广播名称
 */
@property (nonatomic,strong) NSString *localName;

/**
 *  取得设备名称
 *
 *  @return
 */
- (NSString *)getDeviceName;

@end


#pragma mark -蓝牙操作

@interface BLECentralManager : NSObject

/**
 *  蓝牙当前状态
 */
@property (nonatomic,readonly) BLEState bluetoothState;

/**
 *  中心对象
 */
@property (nonatomic,readonly) CBCentralManager *clientmanager;

/**
 *  单例
 *
 *  @return
 */
+ (BLECentralManager *)shareInstance;

/**
 *  获取第一个连接对象
 *
 *  @return
 */
- (BLEConnector *)getFirstConnector;

#pragma mark setup 扫描

/**
 *  开始扫描(相当于重连)
 */
-(void)startScan;

/**
 *  停止扫描
 */
-(void)stopScan;


#pragma mark block

/**
 *  设置蓝牙状态回调
 *
 *  @param aCompletionBlock 蓝牙状态回调block
 */
- (void)setStateChangedBlock:(BLECentralStateChangedBlock)aStateChangedBlock;

/**
 *  设置扫描到蓝牙设备回调
 *
 *  @param aScanBlock 扫描到蓝牙设备回调
 */
- (void)setDeviceScanBlock:(BLECentralDeviceScanBlock)aScanBlock;

/**
 *  设置扫描到多个蓝牙设备回调
 *
 *  @param aDeviceScanCompletedBlock 扫描到多个蓝牙设备回调
 */
- (void)setDeviceScanCompletionBlock:(BLECentralDeviceScanCompletedBlock)aDeviceScanCompletedBlock;

/**
 *  设置蓝牙连接成功
 *
 *  @param aConnectedCompletedBlock 蓝牙连接成功回调
 */
- (void)setConnectedCompletionBlock:(BLECentralConnectSuccessBlock)aConnectedCompletedBlock;

/**
 *  设置蓝牙连接失败
 *
 *  @param aConnectedCompletedBlock 蓝牙连接失败回调
 */
- (void)setConnectFailedBlock:(BLECentralConnectFailedBlock)aFailedBlock;

/**
 *  设置蓝牙断开连接回调
 *
 *  @param aDisconnectBlock 蓝牙断开连接回调
 */
- (void)setDisConnectBlock:(BLECentralDisconnectBlock)aDisconnectBlock;


#pragma mark setup 连接与取消

/**
 *  连接
 */
-(void)beginConnectWithPeripheral:(CBPeripheral *)peripheral;

/**
 *  取消连接
 */
-(void)cancelConnectWithPeripheral:(CBPeripheral *)peripheral;

/**
 *  取消所有连接
 */
- (void)cancelAllConnect;

@end


#pragma mark -sim卡操作

/**
 *  sim卡操作
 */
@interface BLEKiApiSkySim : NSObject

/**
 *  sim卡与蓝牙是否连接
 *
 *  @return YES:连接 NO:未连接
 */
+ (BOOL)isConnected;

/**
 *  取得sim卡唯一id
 *
 *  @param completed 成功回调
 */
+ (void)simCardIdWithCompleted:(void (^) (NSString *cardId,BOOL success))completed;

/**
 *  申请卡片
 *
 *  @param Address   申请卡片ip地址
 *  @param nPort     端口号
 *  @param imsi      imsi
 *  @param completed 卡片申请成功回调
 */
+ (void)applyKiWithAddress:(NSString *)Address
                      port:(UInt16) nPort
                      imsi:(NSString *)imsi
                   success:(void(^)(NSData *kiData,BOOL success))success;

/**
 *  激活上网
 *
 *  @param kiData    申请卡片得到的kiData
 *  @param success   激活完成回调
 */
+ (void)activeInternetWithKiData:(NSData *)kiData
                         success:(void(^)(BOOL success))success;

/**
 *  断开网络连接
 *
 *  @param completed 断开成功回调
 */
+ (void)disconnect:(void(^)(BOOL success))completed;

/**
 *  切换到主卡（170卡）
 *
 *  @param completed 切换成功回调
 */
+ (void)changeToMainCard:(void(^)(BOOL success))completed;

/**
 *  查询sim卡当前的状态
 *
 *  @param completed 查询成功回调
 */
+ (void)searchSkySimState:(void(^)(BLESkySimState currentSkySimState))aSkySimStateBlock;

@end