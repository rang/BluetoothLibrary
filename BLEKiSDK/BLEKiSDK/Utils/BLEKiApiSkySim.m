//
//  BleKiApiSkySim.m
//  BleKiSDK
//
//  Created by wulanzhou on 16/6/16.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import "BLEKiApiObject.h"
#import "ApplyKi.h"
#import "ActiveSkySim.h"
#import "SearchSkySimState.h"
#import "BLEKiUtils.h"
#import "SkySimCardHandler.h"

@implementation BLEKiApiSkySim

/**
 *  sim卡与蓝牙是否连接
 *
 *  @return YES:连接 NO:未连接
 */
+ (BOOL)isConnected{

    BLEConnector *ft=[BLECentralManager shareInstance].getFirstConnector;
    if (ft&&[ft isConnected]) {
        return YES;
    }
    return NO;
}

/**
 *  取得sim卡唯一id
 *
 *  @param completed 成功回调
 */
+ (void)simCardIdWithCompleted:(void (^) (NSString *cardId,BOOL success))completed{
    
    static SkySimCardHandler *mod=nil;
    mod=[[SkySimCardHandler alloc] initWithCmd:[[SkyCmdGetCardID alloc] init]];
    [mod setSkySimReceiveDataCompletionBlock:^(SkySimCardData *decodeModel) {
        if (decodeModel&&[decodeModel isKindOfClass:[SkyCmdGetCardID class]]) {
            NSLog(@"取得sim卡唯一id成功");
            SkyCmdGetCardID *item=(SkyCmdGetCardID*)decodeModel;
            if (completed) {
                completed([item GetSkySimCardID],YES);
            }
            return;
        }
       
        NSLog(@"取得sim卡唯一id失败，超时");
        
        if (completed) {
            completed(nil,NO);
        }
    }];
    [mod send];

}

/**
 *  申请卡片
 *
 *  @param Address   申请卡片ip地址
 *  @param nPort     端口号
 *  @param imsi      imsi
 *  @param completed 卡片申请成功回调
 */
+ (void)applyKiWithAddress:(NSString *)Address
                      port:(UInt16) nPort
                      imsi:(NSString *)imsi
                   success:(void(^)(NSData *kiData,BOOL success))success{

    
    static ApplyKi *mod=nil;
    mod=[[ApplyKi alloc] init];
    mod.ipAddress=Address;
    mod.ipPort=nPort;
    mod.imsiData=[BLEKiUtils stringToByte:imsi];
    mod.KiDownloadCompletedBlock=success;
    
    //开始申请
    [mod asyncApply];

}

/**
 *  激活上网
 *
 *  @param kiData    申请卡片得到的kiData
 *  @param success   激活完成回调
 */
+ (void)activeInternetWithKiData:(NSData *)kiData
                         success:(void(^)(BOOL success))success{

    static ActiveSkySim *mod=nil;
    mod=[[ActiveSkySim alloc] init];
    mod.kiData=kiData;
    mod.ActiveSkySimCompletedBlock=success;
    
    //激活上网
    [mod asyncActive];
}

/**
 *  断开网络连接
 *
 *  @param completed 断开成功回调
 */
+ (void)disconnect:(void(^)(BOOL success))completed{


    static SkySimCardHandler *mod=nil;
    mod=[[SkySimCardHandler alloc] initWithCmd:[[SkyCmdReleaseCard alloc] init]];
    [mod setSkySimReceiveDataCompletionBlock:^(SkySimCardData *decodeModel) {
        BOOL _success=NO;
        NSString *err=nil;
        if (decodeModel&&[decodeModel isKindOfClass:[SkyCmdReleaseCard class]]) {
            SkyCmdReleaseCard *item=(SkyCmdReleaseCard*)decodeModel;
            if ([item.skySimState success]) { //断开成功
                _success=YES;
                err=@"断开网络成功";
            }else{
                err=[item.skySimState GetCurrentState];
            }
        }else{
            err=@"断开连接失败,原因超时";
        }
        
        NSLog(err);
        
        if (completed) {
            completed(_success);
        }
    }];
    [mod send];
}

/**
 *  切换到主卡（170卡）
 *
 *  @param completed 切换成功回调
 */
+ (void)changeToMainCard:(void(^)(BOOL success))completed{

    static SkySimCardHandler *mod=nil;
    mod=[[SkySimCardHandler alloc] initWithCmd:[SkyCmdChangeCard skyCmdChangeMainCard]];
    [mod setSkySimReceiveDataCompletionBlock:^(SkySimCardData *decodeModel) {
        BOOL _success=NO;
        NSString *err=nil;
        if (decodeModel&&[decodeModel isKindOfClass:[SkyCmdChangeCard class]]) {
            SkyCmdChangeCard *item=(SkyCmdChangeCard*)decodeModel;
            //0x46表示已释放了[item.skySimState success]||item.skySimState.simCardState==0x46
            if ([item.skySimState success]) { //切换到170成功
                _success=YES;
                err=@"切换到主卡成功";
            }else{
                err=[item.skySimState GetCurrentState];
            }
        }else{
            err=@"切换失败，发生超时";
        }
        
        NSLog(err);
        
        if (completed) {
            completed(_success);
        }
    }];
    [mod send];
}


/**
 *  查询sim卡当前的状态
 *
 *  @param completed 查询成功回调
 */
+ (void)searchSkySimState:(void(^)(BLESkySimState currentSkySimState))completed{

    static SearchSkySimState *mod=nil;
    mod=[[SearchSkySimState alloc] init];
    mod.SearchSkySimStateCompletedBlock=completed;
    
    //异步查询
    [mod asyncSearch];
}

@end
