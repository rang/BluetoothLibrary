//
//  FTConnector.m
//  BlueDemo
//
//  Created by wulanzhou on 16/2/17.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import "BLEConnector.h"
#import "BLEKiApiObject.h"
#import "SkySimCardHandler.h"
#import "BLEBaseDataPack.h"

NSString *const  kBLEWritePartialDataNotification   = @"kBLEWritePartialDataNotification";
NSString *const  kBLEReceivePartialDataNotification = @"kBLEReceivePartialDataNotification";

@interface BLEConnector ()<CBPeripheralDelegate>{

    CBCharacteristic *_notifiCharacteristic;
    CBCharacteristic *_writeCharacteristic;
    
    NSMutableArray* dataPackList; //<! 待发送报文列表
    BLEBaseDataPack* sendingDataPack; //<! 当前正在发送报文
}

/**
 *  接收蓝牙服务器返回的数据
 */
@property (nonatomic,copy) BLEConnectorReceivePartialDataBlock receiveDataCompletedBlock;

@end

@implementation BLEConnector

- (id)init{
    
    if (self=[super init]){
        dataPackList = [NSMutableArray arrayWithCapacity:10];
    }
    return self;
}

- (void)dealloc{
    dataPackList = nil;
    _notifiCharacteristic=nil;
    _writeCharacteristic=nil;
    
}

#pragma mark -属性重写
- (void)setFtPeripheral:(CBPeripheral *)peripheral{

    if (_ftPeripheral!=peripheral) {
        _ftPeripheral=peripheral;
        _ftPeripheral.delegate=self;
        [_ftPeripheral discoverServices:nil];
    }
}

/**
 *  表示是否已连接
 *
 *  @return
 */
- (BOOL)isConnected{
    
    if (self.ftPeripheral&&self.ftPeripheral.state==CBPeripheralStateConnected) {
        return YES;
    }
    return NO;
}

#pragma mark -block

/**
 *  设置接收到数据的回调
 *
 *  @param aReceivePartialDataBlock 接收到数据的回调
 */
- (void)setReceiveDataCompletionBlock:(BLEConnectorReceivePartialDataBlock)aReceivePartialDataBlock{
    self.receiveDataCompletedBlock=aReceivePartialDataBlock;
}

#pragma mark -公有方法

/**
 *  发送文本内容
 *
 *  @param message
 */
- (void)sendMessage:(NSString *)message{

    //分段发送
    NSData *sendData=[message dataUsingEncoding:NSUTF8StringEncoding];
    [self sendData:sendData];
}

/**
 * 
    分包发送
 代码变量说明
 1. QD_BLE_SEND_MAX_LEN 支持的最大发送包大小
 2. 有两逻辑有代码重合，可以优化下，
 
 *
 *  @param sendData 要发送的数据 NSData类型
 */
- (void)sendData:(NSData *)msgData{
    
    BLEBaseDataPack *mod=[[BLEBaseDataPack alloc] initWithData:msgData];
    [dataPackList addObject:mod];
    
    //如果当前没有发送数据
    if (!sendingDataPack) {
        [self sendNextDataPack];
    }
}

/**
 *  发送SkySimCardData数据到蓝牙
 *
 *  @param simCardData 发送数据对象
 */
- (void)sendSkySimCardData:(SkySimCardData *)simCardData{
    [self sendData:[simCardData encodeSkySimCardData]];
}

/**
 * @breif 发送下一个数据
 */
- (void)sendNextDataPack{
    sendingDataPack = nil;
    if ([dataPackList count]>0) {
        sendingDataPack=[dataPackList objectAtIndex:0];
        [dataPackList removeObjectAtIndex:0];
        //发送数据
        [self writeData:[sendingDataPack beginSendData]];
    }
}

- (void)writeData:(NSData *)subData{
    if (subData&&self.ftPeripheral&&_writeCharacteristic) {
        [self.ftPeripheral writeValue:subData forCharacteristic:_writeCharacteristic type:CBCharacteristicWriteWithResponse];
    }
}


#pragma mark -CBPeripheralDelegate Methods

- (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray*)invalidatedServices{
    
}

/** The Transfer Service was discovered
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (error) {
        NSLog(@"Error discovering services: %@", [error localizedDescription]);
        [[BLECentralManager shareInstance] cancelConnectWithPeripheral:peripheral];
        return;
    }
    for (CBService *service in peripheral.services) {
        //查找特征
        [peripheral discoverCharacteristics:nil forService:service];
        
    }
}


/** The Transfer characteristic was discovered.
 *  Once this has been found, we want to subscribe to it, which lets the peripheral know we want the data it contains
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    // Deal with errors (if any)
    if (error) {
        [[BLECentralManager shareInstance] cancelConnectWithPeripheral:peripheral];
        return;
    }
    for (CBCharacteristic *characteristic in service.characteristics) {
        if (characteristic.properties==CBCharacteristicPropertyNotify) {
            _notifiCharacteristic=characteristic;
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
        }else{
            _writeCharacteristic=characteristic;
        }
    }
}


/** 
   This callback lets us know more data has arrived via notification on the characteristic
   处理蓝牙发过来的数据
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        return;
    }
    
    
    if ([characteristic.value length]==0) {
        return;
    }
    
    NSLog(@"收到数据 =%@",characteristic.value);
    
    //数据解析处理
    SkySimCardData *mod=nil;
    SkySimCardHeadData *cmd=[SkySimCardHeadData decodeSkySimHeadWithData:characteristic.value];
    if (cmd) {
        mod=[SkySimCardHandler decodeWithCmd:cmd characteristicValue:characteristic.value];
    }
    
    if (mod&&[mod isKindOfClass:[SkyCmdHeartBeat class]]) { //如果是服务器心跳包，则回应
        SkyCmdHeartBeat *beat=(SkyCmdHeartBeat*)mod;
        if (beat.skySimState.simCardState==0x00) {
            NSLog(@"收到蓝牙心跳，并回应");
            SkyCmdHeartBeat *mod=[[SkyCmdHeartBeat alloc] init];
            [self sendSkySimCardData:mod]; //回应心跳包
        }
        
    }else{
        //数据回调处理
        if (self.receiveDataCompletedBlock) {
            self.receiveDataCompletedBlock(mod);
        }
    }
    
    //接收数据完成通知
    [[NSNotificationCenter defaultCenter] postNotificationName:kBLEReceivePartialDataNotification object:mod];
}


/** The peripheral letting us know whether our subscribe/unsubscribe happened or not
    中心读取外设实时数据
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"Error changing notification state: %@", error.localizedDescription);
    }
    // Notification has started
    if (characteristic.isNotifying)
    {
        //NSLog(@"Notification began on %@", characteristic);
    }
    else
    { // Notification has stopped
        // so disconnect from the peripheral
        NSLog(@"Notification stopped on %@.  Disconnecting", characteristic);
        //[self.manager cancelPeripheralConnection:self.peripheral];
    }
}

/*
 * 用于检测中心向外设写数据是否成功
 */
- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{

    if (error) {
        NSLog(@"蓝牙发送数据失败,原因= %@", error.description);
    }else{
        NSLog(@"发送数据成功");
    }
    
    //表示数据全部发送完成
    if (sendingDataPack&&sendingDataPack.isFinished) {
        [self sendNextDataPack];
        return;
    }
    
    //发送下一部分数据
    if (sendingDataPack&&!sendingDataPack.isFinished) {
        [self writeData:[sendingDataPack beginSendData]];
    }
}

@end
