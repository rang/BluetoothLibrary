//
//  FTConnector.h
//  BlueDemo
//
//  Created by wulanzhou on 16/2/17.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "BLEKiApiObject.h"

@class SkySimCardData;


extern NSString *const  kBLEWritePartialDataNotification;    //蓝牙写入数据
extern NSString *const  kBLEReceivePartialDataNotification;  //蓝牙接收数据

//接收到数据回调
typedef void (^BLEConnectorReceivePartialDataBlock) (SkySimCardData *cardData);


/**
 *  表示一个连接对象
 */
@interface BLEConnector : NSObject

/**
 *  外围对象
 */
@property (nonatomic,strong) CBPeripheral *ftPeripheral;

/**
 *  表示是否已连接
 *
 *  @return
 */
- (BOOL)isConnected;

/**
 *  发送SkySimCardData数据到蓝牙设备
 *
 *  @param simCardData 发送数据对象
 */
- (void)sendSkySimCardData:(SkySimCardData *)simCardData;

/**
 *  设置接收到数据的回调
 *
 *  @param aReceivePartialDataBlock 接收到数据的回调
 */
- (void)setReceiveDataCompletionBlock:(BLEConnectorReceivePartialDataBlock)aReceivePartialDataBlock;

@end
