//
//  BaseDataPack.h
//  TCPDemo
//
//  Created by bolin on 15-4-6.
//  Copyright (c) 2015年 com.chuzhong. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 * 数据发送处理
 */
@interface BLEBaseDataPack : NSObject

/**
 *  发送的蓝牙数据
 */
@property (nonatomic,strong) NSData *sendData;

/**
 *  是否发送完成
 */
@property (nonatomic,assign) BOOL isFinished;


/**
 *  初始化
 *
 *  @param msgData 要发送的蓝牙数据
 *
 *  @return
 */
- (id)initWithData:(NSData *)msgData;

/**
 *  取得分段发送的数据
 *
 *  @return
 */
- (NSData *)beginSendData;


@end
