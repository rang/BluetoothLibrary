//
//  SkySimCardHeadCmd.m
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import "SkySimCardHeadData.h"

@implementation SkySimCardHeadData

/**
 *  解码处理
 *
 *  @param characteristicValue 解码数据
 *
 *  @return
 */
+ (id)decodeSkySimHeadWithData:(NSData *)characteristicValue{
    
    if (characteristicValue&&[characteristicValue length]>0) {
        
        Byte *b=(Byte *)[characteristicValue bytes];
        if (b[0]!=0x81) {
            return nil;
        }
        
        int len=0;
        if (b[1]) {
            len=b[1]&0xff;
        }
        
        if (len>0) {
            NSData *subData=[characteristicValue subdataWithRange:NSMakeRange(characteristicValue.length-len, len)];
            b=(Byte *)[subData bytes];
            if (b[0]&&b[0]!=0x81) {
                return nil;
            }
        }
        
        
        return [[self alloc] initWithData:characteristicValue];
        
    }
    return nil;
}

- (id)initWithData:(NSData *)characteristicValue{
    
    if (self=[super init]) {
        
        Byte *b=(Byte *)[characteristicValue bytes];
        
        self.headCmd=b[0];
        
        self.contentLen=b[1]&0xff;
        characteristicValue=[characteristicValue subdataWithRange:NSMakeRange(characteristicValue.length-self.contentLen, self.contentLen)];
        b=(Byte *)[characteristicValue bytes];
        
        self.bodyCmd=b[1];
        self.bodyLen=b[2]&0xff;
        
    }
    
    return self;
}

@end
