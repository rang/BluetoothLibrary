//
//  SkySimCardStateData.h
//  BlueDemo
//
//  Created by wulanzhou on 16/3/15.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SkySimCardData.h"
#import "SkySimCardState.h"

@interface SkySimCardStateData : SkySimCardData

/**
 *  卡状态
 */
@property (nonatomic,strong) SkySimCardState *skySimState;

/**
 *  取得当前卡IMSI号
 *
 *  @return
 */
- (NSString *)GetSkySimCardIMSI;

@end
