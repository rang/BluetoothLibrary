//
//  SkySimCardState.m
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import "SkySimCardState.h"

@implementation SkySimCardState


/**
 * 是否操作成功 (YES:成功 NO:失败)
 *
 *  @return
 */
- (BOOL)success{

    if (self.simCardState==0x66) {
        return YES;
    }
    return NO;
}

/**
 *  取得当前状态说明
 *
 *  @return
 */
- (NSString *)GetCurrentState{
    if (self.simCardState==0x66) {
        return @"操作成功";
    }
    else if (self.simCardState==0x44) {
        return @"请求卡片不存在(申请的卡片数据格式有误)";
    }else if (self.simCardState==0x45) {
        return @"SIM卡正忙(开机未完成/上次切换卡片进行中)";
    }else if (self.simCardState==0x46) {
        return @"虚拟卡状态(无须重复释放卡片)";
    }
    
    return @"操作失败";
}

@end
