//
//  SkySimCardHeadCmd.h
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SkySimCardHeadData : NSObject

/**
 *  头部固定指令0x81
 */
@property (nonatomic,assign) Byte headCmd;

/**
 *  指令数据包字节长度
 */
@property (nonatomic,assign) NSInteger contentLen;

/**
 *  指令码(如:0x34 表示获取sim卡id)
 */
@property (nonatomic,assign) Byte bodyCmd;

/**
 * 指令数据包字节长度
 */
@property (nonatomic,assign) NSInteger bodyLen;


/**
 *  解码处理
 *
 *  @param characteristicValue 解码数据
 *
 *  @return
 */
+ (id)decodeSkySimHeadWithData:(NSData *)characteristicValue;


@end
