//
//  SkySimCardState.h
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  卡状态
 */
@interface SkySimCardState : NSObject

/**
 *  sim卡状态码
 */
@property (nonatomic,assign) Byte simCardState;

/**
 * 是否操作成功 (YES:成功 NO:失败)
 *
 *  @return
 */
- (BOOL)success;

/**
 *  取得当前状态说明
 *
 *  @return
 */
- (NSString *)GetCurrentState;

@end
