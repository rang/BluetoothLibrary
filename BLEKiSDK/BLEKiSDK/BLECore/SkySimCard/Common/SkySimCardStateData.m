//
//  SkySimCardStateData.m
//  BlueDemo
//
//  Created by wulanzhou on 16/3/15.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import "SkySimCardStateData.h"

@implementation SkySimCardStateData


#pragma mark -方法重写
- (id)initWithData:(NSData *)characteristicValue{
    
    if (self=[super init]) {
        
        Byte *b=(Byte *)[characteristicValue bytes];
        
        self.headCmd=b[0];
        //总的数据包长度
        self.contentLen=b[1]&0xff;
        
        characteristicValue=[characteristicValue subdataWithRange:NSMakeRange(characteristicValue.length-self.contentLen, self.contentLen)];
        b=(Byte *)[characteristicValue bytes];
        //指令码
        self.bodyCmd=b[1];
        //接收数据长度
        self.bodyLen=b[2]&0xff;
        
        if (!self.skySimState) {
            self.skySimState=[[SkySimCardState alloc] init];
        }
        //状态码
        self.skySimState.simCardState=b[3];
        
        //NSLog(@"状态码:%0.2hhx",self.skySimState.simCardState);
        //接收数据
        if (self.skySimState.simCardState==0x66) {
            if (self.bodyLen-1==0) {
                self.bodyData=nil;
            }else{
                self.bodyData=[characteristicValue subdataWithRange:NSMakeRange(characteristicValue.length-self.bodyLen+1, self.bodyLen-1)];
            }
        }else{
            self.bodyData=nil;
        }
    }
    
    return self;
}

/**
 *  取得当前卡IMSI号
 *
 *  @return
 */
- (NSString *)GetSkySimCardIMSI{
    return [self bodyDataToHexString];
}

@end
