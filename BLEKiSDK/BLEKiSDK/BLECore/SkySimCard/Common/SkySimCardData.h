//
//  SkySimCardBaseData.h
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SkySimCardHeadData.h"


@interface SkySimCardData : SkySimCardHeadData

/**
 *  指令内容
 */
@property (nonatomic,strong) NSData *bodyData;

/**
 *  解码处理
 *
 *  @param characteristicValue 需要解码的数据
 *
 *  @return
 */
- (id)initWithData:(NSData *)characteristicValue;

/**
 *  蓝牙解码处理
 *
 *  @param characteristicValue 需要解码的数据
 *
 *  @return
 */
+ (id)decodeSkySimCardWithData:(NSData *)characteristicValue;

/**
 *  蓝牙编码处理
 *
 *  @return
 */
- (NSData *)encodeSkySimCardData;

/**
 *  指令内容转换成16进制字符串
 *
 *  @return
 */
- (NSString *)bodyDataToHexString;
@end
