//
//  SkySimCardHandler.h
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEKiApiObject.h"
#import "BLEConnector.h"
#import "SkySimCardHeadData.h"
#import "SkyCmdGetCardID.h"
#import "SkyCmdDownloadCardData.h"
#import "SkyCmdChangeCard.h"
#import "SkyCmdReleaseCard.h"
#import "SkyCmdHeartBeat.h"
#import "SkyCmdSearchCardData.h"


/**
 *  蓝牙数据桥接
 */
@interface SkySimCardHandler : NSObject

#pragma mark -数据解析

/**
 *  蓝牙数据解析处理
 *
 *  @param cmd            头命令
 *  @param characterValue 解析数据
 *
 *  @return
 */
+ (SkySimCardData *)decodeWithCmd:(SkySimCardHeadData *)cmd characteristicValue:(NSData *)characterValue;


#pragma mark -数据发送

/**
 *  初始化
 *
 *  @param cmdSkyCardData 要发送的指令对象
 *
 *  @return
 */
- (id)initWithCmd:(SkySimCardData *)cmdSkyCardData;

/**
 *  设置接收数据回调
 *
 *  @param completed 数据回调
 */
- (void)setSkySimReceiveDataCompletionBlock:(void (^) (SkySimCardData *decodeModel))completed;

/**
 *  发送数据
 */
- (void)send;

@end



