//
//  SkyCmdGetCardID.h
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SkySimCardData.h"

/**
 *  读取CARDID
 */
@interface SkyCmdGetCardID : SkySimCardData

/**
 *  取得SkySimID值
 *
 *  @return
 */
- (NSString *)GetSkySimCardID;

@end
