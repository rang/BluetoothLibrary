//
//  SkyCmdDownloadCardData.h
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SkySimCardStateData.h"

/**
 *  下载SIM卡参数[也就是将ki写入数据库] (下载0x82数据)
 */
@interface SkyCmdDownloadCardData : SkySimCardStateData

/**
 *  初始化
 *
 *  @param kiData ki数据
 *
 *  @return
 */
- (id)initWithKiData:(NSData *)kiData;

@end
