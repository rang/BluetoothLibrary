//
//  SkyCmdChangeCard.m
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import "SkyCmdChangeCard.h"

@implementation SkyCmdChangeCard

- (id)init{
    
    if (self=[super init]) {
        self.bodyCmd=0x37;
        
        Byte b[1];
        b[0]=0x92;
        self.bodyData=[[NSData alloc] initWithBytes:b length:sizeof(b)];
        
        //卡状态
        self.skySimState=[[SkySimCardState alloc] init];
    }
    return self;
}

/**
 *  激活那张卡 (8X KI模式,X-代表第X张卡序号 (X<10))
 *
 *  @param cmd  0x80:主号(170号码段)  0x81:静态卡1(Ki下载) 0x82:静态卡2(Ki下载) 0x03:动态卡(远程鉴权)
 */
- (void)changeCardWithByte:(Byte)cmd{
    Byte b[1];
    b[0]=cmd;
    self.bodyData=[[NSData alloc] initWithBytes:b length:sizeof(b)];
}

/**
 *  切换到主卡指令
 *
 *  @return
 */
+ (id)skyCmdChangeMainCard{

    SkyCmdChangeCard *mod=[[SkyCmdChangeCard alloc] init];
    [mod changeCardWithByte:0x80];
    return mod;
}


@end
