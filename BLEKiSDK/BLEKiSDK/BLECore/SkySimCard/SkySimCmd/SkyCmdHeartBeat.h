//
//  SkyCmdHeartBeat.h
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SkySimCardStateData.h"

/**
 *  蓝牙心跳
 */
@interface SkyCmdHeartBeat : SkySimCardStateData

@end
