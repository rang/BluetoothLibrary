//
//  SkyCmdHeartBeat.m
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import "SkyCmdHeartBeat.h"

@implementation SkyCmdHeartBeat

- (id)init{
    
    if (self=[super init]) {
        self.bodyCmd=0x42;
        
        Byte b[1];
        b[0]=0x66;
        self.bodyData=[[NSData alloc] initWithBytes:b length:sizeof(b)];
        
    }
    return self;
}
@end
