//
//  SkyCmdChangeCard.h
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SkySimCardStateData.h"

/**
 *  激活卡(默认激活0x92)
 */
@interface SkyCmdChangeCard : SkySimCardStateData

/**
 *  切换到主卡指令
 *
 *  @return
 */
+ (id)skyCmdChangeMainCard;


@end
