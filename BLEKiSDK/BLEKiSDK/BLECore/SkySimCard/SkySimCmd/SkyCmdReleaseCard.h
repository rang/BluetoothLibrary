//
//  SkyCmdReleaseCard.h
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SkySimCardStateData.h"

/**
 *  释放SIM卡
 */
@interface SkyCmdReleaseCard : SkySimCardStateData


@end
