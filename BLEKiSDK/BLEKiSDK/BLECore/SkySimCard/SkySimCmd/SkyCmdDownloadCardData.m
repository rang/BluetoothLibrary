//
//  SkyCmdDownloadCardData.m
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import "SkyCmdDownloadCardData.h"

@implementation SkyCmdDownloadCardData

- (id)init{
    
    if (self=[super init]) {
        self.bodyCmd=0x35;
        //卡状态
        self.skySimState=[[SkySimCardState alloc] init];
    }
    return self;
}

/**
 *  初始化
 *
 *  @param kiData ki数据
 *
 *  @return
 */
- (id)initWithKiData:(NSData *)kiData{


    if (self=[super init]) {
        
        self.bodyCmd=0x35;
        //卡状态
        self.skySimState=[[SkySimCardState alloc] init];
        
        
        //参数
        NSMutableData *mulData=[NSMutableData dataWithCapacity:1024];
        Byte b[1];
        b[0]=0x82;
        [mulData appendBytes:b length:sizeof(b)];
        [mulData appendData:kiData];
        self.bodyData=mulData;
        
        
    }
    return self;
}



@end
