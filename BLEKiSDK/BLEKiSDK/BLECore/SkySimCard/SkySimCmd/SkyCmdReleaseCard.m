//
//  SkyCmdReleaseCard.m
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import "SkyCmdReleaseCard.h"

@implementation SkyCmdReleaseCard

- (id)init{
    
    if (self=[super init]) {
        self.bodyCmd=0x38;
        
        Byte b[1];
        b[0]=0xFF;
        self.bodyData=[[NSData alloc] initWithBytes:b length:sizeof(b)];
        
        //卡状态
        self.skySimState=[[SkySimCardState alloc] init];
        
    }
    return self;
}
@end
