//
//  SkyCmdSearchCardData.m
//  CoolFlow
//
//  Created by wulanzhou on 16/4/27.
//  Copyright © 2016年 admin. All rights reserved.
//

#import "SkyCmdSearchCardData.h"

@implementation SkyCmdSearchCardData
- (id)init{
    
    if (self=[super init]) {
        self.bodyCmd=0x47;
        
        Byte b[1];
        b[0]=0x00;
        self.bodyData=[[NSData alloc] initWithBytes:b length:sizeof(b)];
        
        //卡状态
        self.skySimState=[[SkySimCardState alloc] init];
        
    }
    return self;
}


#pragma mark -方法重写
- (id)initWithData:(NSData *)characteristicValue{
    
    if (self=[super init]) {
        
        Byte *b=(Byte *)[characteristicValue bytes];
        
        self.headCmd=b[0];
        //总的数据包长度
        self.contentLen=b[1]&0xff;
        
        characteristicValue=[characteristicValue subdataWithRange:NSMakeRange(characteristicValue.length-self.contentLen, self.contentLen)];
        b=(Byte *)[characteristicValue bytes];
        //指令码
        self.bodyCmd=b[1];
        //接收数据长度
        self.bodyLen=b[2]&0xff;
        
        if (!self.skySimState) {
            self.skySimState=[[SkySimCardState alloc] init];
        }
        //状态码
        self.skySimState.simCardState=b[3];
        
        //接收数据
        if (self.bodyLen-1==0) {
            self.bodyData=nil;
        }else{
            self.bodyData=[characteristicValue subdataWithRange:NSMakeRange(characteristicValue.length-self.bodyLen+1, self.bodyLen-1)];
        }
    }
    
    return self;
}


/**
 *  查询主卡指令
 *
 *  @return
 */
+ (id)searchMainCardCmdData{

    SkyCmdSearchCardData *mod=[[SkyCmdSearchCardData alloc] init];
    [mod searchSkyCmdWithIndex:0x00];
    return mod;
}


/**
 *  查询卡片对应索引数据状态
 *
 *  @param cmdByte 对应索引
 */
- (void)searchSkyCmdWithIndex:(Byte)cmdByte{
    self.bodyCmd=0x46;
    
    Byte b[1];
    b[0]=cmdByte;
    self.bodyData=[[NSData alloc] initWithBytes:b length:sizeof(b)];
    
}


@end
