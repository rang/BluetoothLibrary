//
//  SkyCmdGetCardID.m
//  BlueDemo
//
//  Created by wulanzhou on 16/3/14.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import "SkyCmdGetCardID.h"

@implementation SkyCmdGetCardID

- (id)init{

    if (self=[super init]) {
        self.bodyCmd=0x34;
        
        Byte b[1];
        b[0]=0x00;
        self.bodyData=[[NSData alloc] initWithBytes:b length:sizeof(b)];
        
    }
    return self;
}

/**
 *  取得SkySimID值
 *
 *  @return
 */
- (NSString *)GetSkySimCardID{

    return [self bodyDataToHexString];
}

@end
