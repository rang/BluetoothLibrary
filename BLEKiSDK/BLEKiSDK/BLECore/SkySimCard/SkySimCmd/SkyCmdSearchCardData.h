//
//  SkyCmdSearchCardData.h
//  CoolFlow
//
//  Created by wulanzhou on 16/4/27.
//  Copyright © 2016年 admin. All rights reserved.
//

#import "SkySimCardStateData.h"

/**
 *  查询sim卡状态
 *  0x46 查询卡片对应索引数据状态
 *  0x47 获取卡片当前使用数据状态 (默认查询0x47)
 */
@interface SkyCmdSearchCardData : SkySimCardStateData

/**
 *  查询主卡指令
 *
 *  @return
 */
+ (id)searchMainCardCmdData;


@end
