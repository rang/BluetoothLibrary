//
//  BleKiUtils.h
//  BleKiSDK
//
//  Created by wulanzhou on 16/6/16.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kBLELocalChannelKey  @"kBLELocalChannelKey"
#define kBLELocalShowLogKey  @"kBLELocalShowLogKey"

@interface BLEKiUtils : NSObject


/**
 *  获取日志文件路径
 *
 *  @return
 */
+ (NSString *)getLogFilePath;


/**
 *  判断是否可以写日志
 */
+ (BOOL)isWriteLogEnable;


/**
 *  二进制字符串转化为NData
 *
 *  @param string 二进制字符串
 *
 *  @return
 */
+ (NSData *)stringToByte:(NSString*)string;


/**
 *  设置缓存值
 *
 *  @param object 缓存值
 *  @param key    缓存key
 */
+ (void)setValue:(id)object  forKey:(NSString*)key;


/**
 *  设置缓存值
 *
 *  @param boo    缓存值
 *  @param key    缓存key
 */
+ (void)setBool:(BOOL)boo  forKey:(NSString*)key;

@end
