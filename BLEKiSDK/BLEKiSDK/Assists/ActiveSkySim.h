//
//  ActiveSkySim.h
//  BleKiSDK
//
//  Created by wulanzhou on 16/6/16.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  激活sim卡
 */
@interface ActiveSkySim : NSObject

/**
 *  ki数据
 */
@property (nonatomic,strong) NSData  *kiData;

/**
 *  激活成功回调
 */
@property (nonatomic,copy) void (^ActiveSkySimCompletedBlock) (BOOL success);


/**
 *  激活
 */
- (void)asyncActive;

@end
