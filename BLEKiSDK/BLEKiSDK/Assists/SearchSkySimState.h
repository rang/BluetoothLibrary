//
//  SearchSkySimState.h
//  BleKiSDK
//
//  Created by wulanzhou on 16/6/16.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEKiApiObject.h"

/**
 *  查询skysim卡状态
 */
@interface SearchSkySimState : NSObject

/**
 *  查询成功回调
 */
@property (nonatomic,copy) void (^SearchSkySimStateCompletedBlock) (BLESkySimState currentSkySimState);


/**
 *  异步查询
 */
- (void)asyncSearch;

@end
