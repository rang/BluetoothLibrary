//
//  ApplyKi.h
//  BleKiSDK
//
//  Created by wulanzhou on 16/6/16.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  申请ki
 */
@interface ApplyKi : NSObject

/**
 *  ip地址
 */
@property (nonatomic,strong) NSString *ipAddress;

/**
 *  ip端口号
 */
@property (nonatomic,assign) UInt16   ipPort;

/**
 *  imsi数据
 */
@property (nonatomic,strong) NSData  *imsiData;


/**
 *  ki下载完成回调
 */
@property (nonatomic,copy) void (^KiDownloadCompletedBlock) (NSData *kiData,BOOL success);


/**
 *  开始申请
 */
- (void)asyncApply;

@end
