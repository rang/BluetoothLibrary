//
//  ActiveSkySim.m
//  BleKiSDK
//
//  Created by wulanzhou on 16/6/16.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import "ActiveSkySim.h"
#import "SkySimCardHandler.h"

@interface ActiveSkySim (){
    
    BOOL isLoading;
    
}
@end

@implementation ActiveSkySim

- (id)init{
    
    if (self=[super init]) {
        isLoading=NO;
    }
    return self;
}

/**
 *  激活
 */
- (void)asyncActive{

    if ([BLECentralManager shareInstance].bluetoothState!=BLEStateOpen) {
        
        [self activeFinishWithSuccess:NO message:@"蓝牙未打开，请确保打开后再试。"];
        
        return;
    }
    
    if (![[BLECentralManager shareInstance].getFirstConnector isConnected]) {
        [self activeFinishWithSuccess:NO message:@"未能连接SkySim，请稍后再试。"];
        return;
    }
    
    if (isLoading) {
        return;
    }
    
    if (self.kiData&&[self.kiData isKindOfClass:[NSData class]]&&[self.kiData length]>0) {
        isLoading=YES;
        
        [self downloadKiToSkySimCompleted:^(BOOL success) {
            if (success) {
                [self oneInternet];
            }
        }];
        
        
    }else{
        [self activeFinishWithSuccess:NO message:@"参数不能为nil"];
    }

}

/**
 *  一键激活上网
 */
- (void)oneInternet{

    static SkySimCardHandler *mod=nil;
    mod=[[SkySimCardHandler alloc] initWithCmd:[[SkyCmdChangeCard alloc] init]];
    [mod setSkySimReceiveDataCompletionBlock:^(SkySimCardData *decodeModel) {
        BOOL _success=NO;
        NSString *err=nil;
        if (decodeModel&&[decodeModel isKindOfClass:[SkyCmdChangeCard class]]) {
            SkyCmdChangeCard *item=(SkyCmdChangeCard*)decodeModel;
            if ([item.skySimState success]) { //激活成功
                _success=YES;
                err=@"激活成功";
                
            }else{
                err=[item.skySimState GetCurrentState];
            }
        }else{
            err=@"一键上网失败,发生超时";
        }
        
        [self activeFinishWithSuccess:_success message:err];
    }];
    [mod send];
}

/**
 *  ki写入到skysim卡
 *
 *  @param completed 成功回调
 */
- (void)downloadKiToSkySimCompleted:(void (^) (BOOL success))completed{

    //将ki写入sim卡
    SkyCmdDownloadCardData *sendCmd=[[SkyCmdDownloadCardData alloc] initWithKiData:self.kiData];
    static SkySimCardHandler *mod=nil;
    mod=[[SkySimCardHandler alloc] initWithCmd:sendCmd];
    [mod setSkySimReceiveDataCompletionBlock:^(SkySimCardData *decodeModel){
        
        BOOL writeSuccess=NO;
        
        if (decodeModel&&[decodeModel isKindOfClass:[SkyCmdDownloadCardData class]]) {
            SkyCmdDownloadCardData *item=(SkyCmdDownloadCardData*)decodeModel;
            if ([item.skySimState success]) { //ki写入成功
                NSLog(@"ki写入成功");
                //激活上网
                writeSuccess=YES;
                
                
            }else{
                [self activeFinishWithSuccess:NO message:[NSString stringWithFormat:@"写入ki失败，状态码为:%0.2hhx",item.skySimState.simCardState]];
            }
        }else{
            [self activeFinishWithSuccess:NO message:@"写入ki失败，发生超时"];
        }
        
        
        if (completed) {
            completed(writeSuccess);
        }
        
    }];
    [mod send];
}

- (void)activeFinishWithSuccess:(BOOL)success message:(NSString *)msg{
    NSLog(msg);
    isLoading=NO;
    if (self.ActiveSkySimCompletedBlock) {
        self.ActiveSkySimCompletedBlock(success);
    }
}
@end
