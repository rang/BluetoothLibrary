//
//  SearchSkySimState.m
//  BleKiSDK
//
//  Created by wulanzhou on 16/6/16.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import "SearchSkySimState.h"
#import "SkySimCardHandler.h"


@interface SearchSkySimState (){
    
    BOOL isLoading;
    
}
@end


@implementation SearchSkySimState


- (id)init{
    
    if (self=[super init]) {
        isLoading=NO;
    }
    return self;
}


/**
 *  异步查询
 */
- (void)asyncSearch{

    if ([BLECentralManager shareInstance].bluetoothState!=BLEStateOpen) {
        
        [self searchFinishWithState:BLESkySimStateUnKnown success:NO message:@"蓝牙未打开，请确保打开后再试。"];
        
        return;
    }
    
    if (![[BLECentralManager shareInstance].getFirstConnector isConnected]) {
        [self searchFinishWithState:BLESkySimStateUnKnown success:NO message:@"未能连接SkySim，请稍后再试。"];
        return;
    }
    
    if (isLoading) {
        return;
    }
    
    //查询主卡状态
    [self searchMainCardState];

}


/**
 *  查询主卡状态
 */
- (void)searchMainCardState{
    
    NSLog(@"查询主卡状态46");
    static SkySimCardHandler *mod=nil;
    mod=[[SkySimCardHandler alloc] initWithCmd:[SkyCmdSearchCardData searchMainCardCmdData]];
    [mod setSkySimReceiveDataCompletionBlock:^(SkySimCardData *decodeModel) {
        if (decodeModel&&[decodeModel isKindOfClass:[SkyCmdSearchCardData class]]){
            SkyCmdSearchCardData *item=(SkyCmdSearchCardData*)decodeModel;
            if (item.bodyCmd==0x46) {
                NSString *strIMSI=[item GetSkySimCardIMSI];
                if (item.skySimState.simCardState==0x01&&[strIMSI length]>0) {
                    
                    NSLog(@"开始查询当前卡片状态47");
                    //[CacheDataUtil setValue:strIMSI forKey:key];
                    
                    [self compareSkySimStateWithIMSI:strIMSI];
                    
                }else{
                    
                    NSLog(@"取得0卡槽170号码的imsi失败,失败状态码为:%0.2hhx",item.skySimState.simCardState);
                    
                    [self searchFinishWithState:BLESkySimStateUnKnown success:NO message:[NSString stringWithFormat:@"查询主卡状态失败，失败状态码为:%0.2hhx",item.skySimState.simCardState]];
                }
            }
        }else{
            NSLog(@"取得0卡槽170号码的imsi失败,发生超时!");
            [self searchFinishWithState:BLESkySimStateUnKnown success:NO message:@"查询主卡状态失败，发生超时!"];
        }
    }];
    [mod send];
}

/**
 *  比较0卡槽170号码的imsi与当前卡片数据状态IMSI是否相同
 *
 *  @param simIMSI  0卡槽170号码的imsi
 */
- (void)compareSkySimStateWithIMSI:(NSString *)simIMSI{
    static SkySimCardHandler *mod=nil;
    mod=[[SkySimCardHandler alloc] initWithCmd:[[SkyCmdSearchCardData alloc] init]];
    [mod setSkySimReceiveDataCompletionBlock:^(SkySimCardData *decodeModel) {
        if (decodeModel&&[decodeModel isKindOfClass:[SkyCmdSearchCardData class]]){
            
            SkyCmdSearchCardData *item=(SkyCmdSearchCardData*)decodeModel;
            if (item.bodyCmd==0x47) {
                //item.skySimState.simCardState状态码说明:01表示ki 00表示动态 ff表示处理虚拟卡状态已释放过了
                NSString *strIMSI=[item GetSkySimCardIMSI];
                if ([strIMSI length]>0) {
                    if ([strIMSI isEqualToString:simIMSI]||item.skySimState.simCardState==0xff) { //处理虚拟卡状态
                        
                        BLESkySimState currentState;
                        
                        if (item.skySimState.simCardState==0xff) {
                            NSLog(@"当前sim卡为虚拟卡状态,表示上一次网络断开,是由服务器主动断开");
                            currentState=BLESkySimStateVirtualCard;
                        }else{
                            currentState=BLESkySimStateMainCard;
                            NSLog(@"当前sim卡为170状态");
                        }
                        
                        [self searchFinishWithState:currentState success:YES message:@"查询成功"];
                        
                    }else{
                        NSLog(@"当前sim卡为ki状态");
                        
                        [self searchFinishWithState:BLESkySimStateNetworkCard success:YES message:@"查询成功"];
                    }
                    
                }else{
                    
                    NSLog(@"获取卡片当前使用数据状态失败,状态码为:%0.2hhx",item.skySimState.simCardState);
                    
                    [self searchFinishWithState:BLESkySimStateUnKnown success:NO message:[NSString stringWithFormat:@"查询当前卡片状态失败，失败状态码为:%0.2hhx",item.skySimState.simCardState]];
                }
            }
        }else{
            NSLog(@"查询sim卡状态失败，发生超时!!");
            [self searchFinishWithState:BLESkySimStateUnKnown success:NO message:@"查询当前卡片状态失败，发生超时!"];
        }
    }];
    [mod send];
}


- (void)searchFinishWithState:(BLESkySimState)state success:(BOOL)success message:(NSString *)msg{
    
    isLoading=NO;
    if (self.SearchSkySimStateCompletedBlock) {
        self.SearchSkySimStateCompletedBlock(state);
    }
}

@end
