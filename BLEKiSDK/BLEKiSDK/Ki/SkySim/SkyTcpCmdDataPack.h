//
//  SkyTcpCmdDataPack.h
//  BLEKiSDK
//
//  Created by wulanzhou on 16/3/16.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SkyTcpCmdDataPack : NSObject

/**********************短包区************************/

/**
*  总包长度 (4位)
*/
@property (nonatomic,assign) NSInteger totalDataPackLen;

/**
 *  长包时也协议版本号 (默认值DF 1位)
 */
@property (nonatomic,assign) Byte packVersion;

/**
 *  上行报文标志(APP-->ASC) (默认值55 1位)
 */
@property (nonatomic,assign) Byte ascSign;

/**********************命令区************************/

/**
 *  使用CardID号 (8位)
 */
@property (nonatomic,strong) NSData *cardData;

/**
 *  主命令参数 (默认值0x01 1位)
 */
@property (nonatomic,assign) Byte mCmd;

/**
 *  子命令参数 (默认值0x01 1位)
 */
@property (nonatomic,assign) Byte subCmd;

/**********************数据区长度************************/

/**
 *  数据区长度 (2位)
 */
@property (nonatomic,assign) short udataPackLen;

/**
 *  状态码 (0x0000:成功 0x0260:其他错误[重新选择卡参数])
 */
@property (nonatomic,strong) NSData *stateData;

/**********************数据区************************/

/**
 *  包序列号 (4位 随意)
 */
@property (nonatomic,strong) NSData *packSN;

/**
 *  OEMID当前厂家编码 (默认值0x07D1 2位)
 */
@property (nonatomic,strong) NSData *apkSN;

/**
 *  应用编号 (默认值0x03E9 2位)
 */
@property (nonatomic,strong) NSData *appSN;

/**
 *   命令参数 (组成结构:0x910180[区分标识]+0x98+IMSI字节长度+IMSI数据)
 */
@property (nonatomic,strong) NSData *cmdData;

/**********************MAC区************************/

/**
 *   MAC (固定值 0xFEFEFEFE)
 */
@property (nonatomic,strong) NSData *macData;


- (id)initWithDataPack:(NSData *)dataPack;

/**
 *  tcp数据包解码处理
 *
 *  @param dataPack 解码数据包
 *
 *  @return
 */
+ (id)decodeWithDataPack:(NSData *)dataPack;

/**
 *  tcp数据包编码处理
 *
 *  @return
 */
- (NSData *)encodeDataPack;

/**
 *  下载ki需要添加数据（也就是ki申请）
 *
 *  @param cardIdData CardID号
 *  @param imsiData   IMSI号(服务器下载取得的imsi)
 */
- (void)downloadKiWithCardIDData:(NSData *)cardIdData withIMSIData:(NSData *)imsiData;

/**
 *  释放ki数据
 *
 *  @param cardIdData CardID号
 *  @param imsiData   IMSI号 (ble释放sim卡时取得的imsi)
 */
- (void)releaseKiWithCardIDData:(NSData *)cardIdData withIMSIData:(NSData *)imsiData;

/**
 *  判断是否为释放ki
 *
 *  @return YES:是 NO:不是
 */
- (BOOL)isReleaseKiData;

/**
 *  判断ki是否下载成功
 *
 *  @return
 */
- (BOOL)success;

/**
 *  取得ki数据
 *
 *  @return
 */
- (NSData *)GetKiData;

@end
