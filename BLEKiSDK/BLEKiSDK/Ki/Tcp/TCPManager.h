//
//  TCPManager.h
//  CoolFlow
//
//  Created by admin on 16/3/14.
//  Copyright © 2016年 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SkyTcpCmdDataPack.h"
@interface TCPManager : NSObject

//instance
+ (TCPManager *)shareInstance;

/**
 *  get KI or release ki
 *
 *  @param strIP     服务器地址
 *  @param nPort     端号
 *  @param dataPack  发送数据
 *  @param completed 取到ki数据的回调
 */
- (void)getKIFrom:(NSString *) strIP withPort:(UInt16) nPort skyTcpCmdData:(SkyTcpCmdDataPack *) dataPack dataPackReceiveCompleted:(void(^)(SkyTcpCmdDataPack *decodeTcpDataPack))completed;

@end
