//
//  SkySimViewController.m
//  ExampleDemo
//
//  Created by wulanzhou on 16/6/16.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import "SkySimViewController.h"
#import "BLEKiApiObject.h"
#import "SVProgressHUD.h"
#import "CacheDataUtil.h"

#define kTestKiDataKey    @"kTestKiDataKey"

@interface SkySimViewController ()

@end

@implementation SkySimViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title=@"上网操作";
    
    
    //取得sim cardId
    [BLEKiApiSkySim simCardIdWithCompleted:^(NSString *cardId, BOOL success) {
        
        if (success) {
            NSLog(@"取得cardId =%@",cardId);
        }else{
            NSLog(@"取得sim cardId失败");
        }
    }];
    
    
    //查询当前卡状态
    [BLEKiApiSkySim searchSkySimState:^(BLESkySimState currentSkySimState) {
        
        if (currentSkySimState==BLESkySimStateUnKnown) {
            NSLog(@"未知状态");
        }else if (currentSkySimState==BLESkySimStateMainCard){
           NSLog(@"主卡状态（有带手机号码的sim卡）");
        }else if (currentSkySimState==BLESkySimStateVirtualCard){
            NSLog(@"虚拟卡状态（表示前一次上网非自已主动断开网络，是由服务器断开的）");
        }else if (currentSkySimState==BLESkySimStateNetworkCard){
            NSLog(@"上网状态");
        }
        
    }];
    
    
    //断开连接(对应通知kBLEConnectCloseNotification)
    [[BLECentralManager shareInstance] setDisConnectBlock:^(CBPeripheral *peripheral, NSError *error) {
        [SVProgressHUD showInView:self.view];
        [SVProgressHUD dismissWithError:@"蓝牙连接已断开" afterDelay:2.0f];
        NSLog(@"连接断开原因=%@",error.description);
        
        //断开连接，可进行重连动作
        [[BLECentralManager shareInstance] startScan];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//申请卡片
- (IBAction)applyKiClick:(id)sender {
    
    //imsi通过购买所得
    
    [SVProgressHUD showInView:self.view];
    [BLEKiApiSkySim applyKiWithAddress:@"14.215.135.10" port:38000 imsi:@"084906705007100200" success:^(NSData *kiData, BOOL success) {
        if (success) {
            
            //保存下载的ki
            [CacheDataUtil setValue:kiData forKey:kTestKiDataKey];
            
            [SVProgressHUD dismissWithSuccess:@"申请成功" afterDelay:2.0f];
        }else{
        
            [SVProgressHUD dismissWithError:@"申请失败" afterDelay:2.0f];
            
        }
    }];
    
}

//一键上网
- (IBAction)oneInternetClick:(id)sender {
    
    NSData *ki=[CacheDataUtil valueForKey:kTestKiDataKey];
    if (ki) {
        
        [SVProgressHUD showInView:self.view];
        [BLEKiApiSkySim activeInternetWithKiData:ki success:^(BOOL success) {
            if (success) {
                [SVProgressHUD dismissWithSuccess:@"一键上网成功" afterDelay:2.0f];
            }else{
                
                [SVProgressHUD dismissWithError:@"一键上网失败" afterDelay:2.0f];
            }
        }];
        
    }else{
    
        [SVProgressHUD showInView:self.view];
        [SVProgressHUD dismissWithError:@"请先申请卡片" afterDelay:2.0f];
    }
    
}

//断开连接
- (IBAction)closeInternetClick:(id)sender {
    
    [SVProgressHUD showInView:self.view];
    [BLEKiApiSkySim disconnect:^(BOOL success) {
        if (success) {
            [SVProgressHUD dismissWithSuccess:@"断开成功" afterDelay:2.0f];
        }else{
            [SVProgressHUD dismissWithError:@"断开失败" afterDelay:2.0f];
        }
    }];
    
    //注：也可以通过切换到主卡的方式来断开连接
    
}

//切换到主卡
- (IBAction)changeCardCllick:(id)sender {
    [SVProgressHUD showInView:self.view];
    [BLEKiApiSkySim changeToMainCard:^(BOOL success) {
        if (success) {
            [SVProgressHUD dismissWithSuccess:@"切换成功" afterDelay:2.0f];
        }else{
            [SVProgressHUD dismissWithError:@"切换失败" afterDelay:2.0f];
        }
    }];
}
@end
