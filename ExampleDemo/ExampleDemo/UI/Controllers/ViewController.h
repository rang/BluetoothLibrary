//
//  ViewController.h
//  ExampleDemo
//
//  Created by wulanzhou on 16/6/15.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *deviceTable;

@property (nonatomic,strong) NSArray *listData;


//扫描
- (IBAction)startScanClick:(id)sender;

@end

