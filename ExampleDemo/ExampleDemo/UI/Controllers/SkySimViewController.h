//
//  SkySimViewController.h
//  ExampleDemo
//
//  Created by wulanzhou on 16/6/16.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SkySimViewController : UIViewController


//申请卡片
- (IBAction)applyKiClick:(id)sender;

//一键上网
- (IBAction)oneInternetClick:(id)sender;

//断开连接
- (IBAction)closeInternetClick:(id)sender;

//切换到主卡
- (IBAction)changeCardCllick:(id)sender;


@end
