//
//  AppDelegate.h
//  ExampleDemo
//
//  Created by wulanzhou on 16/6/15.
//  Copyright © 2016年 wulanzhou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

